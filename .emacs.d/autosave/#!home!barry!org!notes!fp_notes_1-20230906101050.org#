:PROPERTIES:
:ID:       ce21695a-8f95-496b-afa9-3950ada4bff2
:END:
#+STARTUP:latexpreview
#+STARTUP:inlineimages
 #+date: [2023-09-06 Wed 10:48]
 #+title:fp notes 1
 backlink: [[id:088fdb2a-b9bc-4784-8c11-7785953d54e2][functional programming]]
 tags:

* fp notes lecture 2
** what is a function
=set theory=: a /univalent/ and /total/ relation
given sets D (domain) and C (codomain), $f \subseteq D \times C$ is a function when:
$$ (\forall a : a \in D ; (\exists r: r \in C : (a, r) \in f))$$ =total=
$( \forall a,r,s : (a,r) \in f \and (a,s) \in f : r = s)$ =univalent=

$ (a,r)$ \in $f$ written as $f.a = r$ f applied to argument a results in r
** functions in functional programming
functional program = collection of function
functions are =typed=: types is intrinsic to function
if $f \in X \rightarrow Y$ and $g \in X' \rightarrow Y'$, we have f=g when:
- $X = X'$ and, domains equal
- $Y = Y'$ and, codomains equal
- $f.x = g.x$ for all x in X =extensionality=
** what is a type
(data)type = a collection of related values and operations
-> OO programming: value = object from a class, operations = methods
- it doesnt matter whats inside the type
- it matters what is the =structure= of the data
- structure is captured by functions! (ofc, duh)

** basic concepts and notation
function =type=: $f \in \alpha \rightarrow \beta$ (domain $\alpha$, codomain $\beta$, function space $\alpha \rightarrow \beta$)
function =application=: $f.a \in \beta$ for all $f \in \alpha \rightarrow \beta$ and $a in \alpha$
=identity= function =id= ($id_\alpha$) is defined by $id \in \alpha \rightarrow \alpha$ with id.x = x
function =composition=: $f \circ g$ is defined for $f \in \gamma \leftarrow \beta$ and $g \in \beta \leftarrow \alpha$ by
$f \circ f \in \gamma \leftarrow \alpha$
$(f \circ g).a = f.(g.a)$ for all $a \in \alpha$
composition has id as =unit=: $id \circ f = f \circ id = f$
composition is =associative=: $(f \circ g) \circ h = f \circ (g \circ h)$

=lambda abstraction=: $(\lambda a: b)$ expression b as nameless function of a
$(\lambda a:b) \in \alpha \rightarrow \beta$ when $a \in \alpha$ and $b \in \beta$
$(\lambda a: b).c = b <a:=c>$ replace free occurrences of a in b by c

| *concept* | *math* | *haskell* | *CFP* |
|         |      |         |     |


*** ways of defining functions
=pointwise=: expression involving dummy arguments: f.x = f. (h.x)
=$\lambda$ abstraction=: expression interpreted as function of given variable
$f = (\lambda x: g. (h.x))$
=composition= $f = g \circ h$
=recursive=: expression where function also occurs on right-hand side


** Curried and uncurried functions
- function of type A -> B -> C is called =curried=
- function of type A x B -> C is called =uncurried=
- currying an uncurried function:
  $curry \in (\alpha \times \beta \rightarrow \gamma) \rightarrow (\alpha \rightarrow \beta \rightarrow \gamma)$
  curry.f.x.y = f.(x,y)
*** infix operators
infix operators are uncurried: $\oplus \in A \times B \rightarrow C#$
$x \oplus y = \oplus .(x,y)$
N.B in haskell infix operators are curried

** evaluation of functional expressions
$mix \in \mathbb{N} \rightarrow \mathbb{N} \rightarrow \mathbb{N}$ by mix.x.y = if x > y then x else mix.y.x
mix.0 = partial application = function
mix.0.1 = evaluated expression
mix.0.(mix.1.2) = reducible expression =REDEX=
- =redex= has form $\phi.a.b.\cdots$ where $\phi$ can be apoplied = all its arguments are present
- expression t is in =normal form= when t has no redexes
- expression t =has a normal form= when t is reducible to a normal form

** church-rosser theorem
theorem (church-rosser, confluence, diamond property)
if $t \rightarrow ^* u_1$ and $t \rightarrow ^* u_2$ then there exists $v$ such that $u_1 \rightarrow ^* v$ and $u_2 \rightarrow ^* v$
in the end, the order of rewrute steps does not matter

=corollary= uniqueness of normal form
if expression t has a normal form then that normal form is =unique=

reduction order is not important for correctness but for efficiency: -> evaluation strategies:
- =normal-order= (call-by-name): leftmost outermost redex first. evaluate function body before its arguments. leads to normal form if it exists but can be inefficient (duplicated work)
- =applicative-order= (call-by-value): leftmost innermost redex first. evaluate arguments before function body. may not terminate, but potentially more efficient.
- =lazy evaluation=: normal-order with *memoization*: sharong, graph reduction


** tail & head recursion
Definition (=tail recursive= function /tr/, given $b \in \alpha \rightarrow \beta, g \in \mathbb{N} \rightarrow \alpha \rightarrow \beta)$
$tr \in \alpha \rightarrow \mathbb{N} \rightarrow \beta$
tr.a.0 = b.a
tr.a.(n+1) = tr.(g.n.a).n
for example, in case of generalized factorial, we have b = id and g,n = (*(n+1))

in haskell to covert from tail to head recursive use flip: hr = flip.tr

to convert:
$hr \in \mathbb{N} \rightarrow \alpha \rightarrow \beta$
hr.n.a = tr.a.n
