from pathlib import Path
import shutil

import pytest

from autodl_datasets.utils.samples import (
    sample_instance_segmentation,
    sample_classification,
    sample_object_detection,
)

from autdl_ultralytics.train import main

SAMPLE_DATASET_NAME = "sample.json"


@pytest.fixture
def sample_dataset(tmpdir, request):
    task = request.param
    if task == "classification":
        dataset = sample_classification(
            dataset_size=200,
            root_dir=tmpdir,
            nr_objects_range=(1, 1),
            columns_as_local_file=["image"],
            purge_root_dir_if_exists=True,
        )
    elif task == "instance_segmentation":
        dataset = sample_instance_segmentation(
            dataset_size=200,
            root_dir=tmpdir,
            nr_objects_range=(1, 10),
            columns_as_local_file=["image"],
            purge_root_dir_if_exists=True,
        )
    elif task == "object_detection":
        dataset = sample_object_detection(
            dataset_size=200,
            root_dir=tmpdir,
            nr_objects_range=(1, 10),
            columns_as_local_file=["image"],
            purge_root_dir_if_exists=True,
        )
    else:
        raise ValueError(f"Invalid task: {task}")

    dataset.meta["uuid"] = "test"
    dataset.save(f"sample_{task}.json")
    yield dataset


@require_torch_gpu
@pytest.mark.slow
@pytest.mark.parametrize("sample_dataset", ["classification"], indirect=True)
def test_cls_train(sample_datset)