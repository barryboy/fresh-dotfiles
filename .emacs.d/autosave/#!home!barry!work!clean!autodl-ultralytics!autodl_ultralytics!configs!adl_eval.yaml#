
context: 'vbti/test/project-b'
job_type: EVAL
experiment_name: 'yolo8 test'

docker:
  image: 'docker.prod.vbti.nl/cloud/autodl-ultralytics-x86:0.2.1'

# resources constraints for the job
resource_constraints:
  cpu_count: 8
  gpu_count: 1

storage:
  # set the environment variables correctly, see https://gitlab.com/vbti-products/autodl/autodl-storage-client/-/blob/main/autodl_storage_client/config/settings.yaml
  download_dataset: true # (bool) download the dataset before evaluation
  download_model: true # (bool) download the model before evaluation
  upload_evaluation: true # (bool) upload the model after training, the name of the model will be set to _run_dir_name_

dataset: # which dataset to use
  name: test_classification_cat_dog # (REQUIRED|str) stored name of the dataset

pred_dataset_suffix: '' # (str) suffix to add to the name of the predicted dataset
force_rerun: false # (str) to rerun the model on the dataset even if the predicted dataset for this model already exists

deployable:
  name: ~ # (str) if set, will use the deployable or create a new one if it does not exist
  # if set and deployable does not exist this will create a new deployable with the given name
  model_config: # required if name is not set, or deployable with name does not exist, ignored otherwise
    name: 'all-drum-0' # (REQUIRED|str) name of the model to use, e.g. '20230327-123313'
    __init_kwargs__:
      # checkpoint_name:
      #task: segment # or detect, classify
      overwrite_framework_config: # (Optional[Dict[str, Any]]) overwrite framework config, e.g. {'model': {'backbone': {'depth': 50}}}
        imgsz: 320 # (int) size of input image
        batch: -1 # number of images per batch (-1 for autobatch)
        save_json: True # save results to JSON file
        save_hybrid: False # save hybrid version of labels (labes + additional predictions)
        conf: 0.001 # object confidence threshold for deection
        iou:  0.6 # intersection over union (IoU) threshold for NMS
        max_det: 1xf00 # maximum number of detections per image
        half: True # use half precision (FP16)
        dnn: False # use OpenCV DNN for ONNX inference
        plots: False # show plots during training
        rect: False # rectangular val with each batch collated for minimum padding
    # __predict_kwargs__: # (Dict[str, Any]) kwargs to pass to the predict method of the model

report: # report config
  template_notebook: 'eval.ipynb' # path to the template notebook
  params: # parameters to pass to the template notebook (defined in the first cell of the notebook)
    CONF_THRESHOLD: 0.05
    METRIC_IOU_THRESHOLD: 0.3
    NMS_IOU_THRESHOLD: 0.3
    BBOX_SURFACE_THRESHOLD: 100
    SHOW_TOP_K_IMAGES: 1
    FILTER_METRIC_IOU_THRESHOLD: 0.3
