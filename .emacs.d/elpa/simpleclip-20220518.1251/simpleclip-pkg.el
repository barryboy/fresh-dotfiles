(define-package "simpleclip" "20220518.1251" "Simplified access to the system clipboard" 'nil :commit "023f239275115169c3a3637ad95fae4a036c005e" :authors
  '(("Roland Walker" . "walker@pobox.com"))
  :maintainers
  '(("Roland Walker" . "walker@pobox.com"))
  :maintainer
  '("Roland Walker" . "walker@pobox.com")
  :keywords
  '("convenience")
  :url "http://github.com/rolandwalker/simpleclip")
;; Local Variables:
;; no-byte-compile: t
;; End:
