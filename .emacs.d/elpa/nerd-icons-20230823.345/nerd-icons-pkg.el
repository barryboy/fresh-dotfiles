(define-package "nerd-icons" "20230823.345" "Emacs Nerd Font Icons Library"
  '((emacs "24.3"))
  :commit "ac9014a34073db26886844cf8c16c76758f8b4c6" :authors
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/rainstormstudio/nerd-icons.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
