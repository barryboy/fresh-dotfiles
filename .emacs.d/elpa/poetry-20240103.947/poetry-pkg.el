(define-package "poetry" "20240103.947" "Interface to Poetry"
  '((transient "0.2.0")
    (pyvenv "1.2")
    (emacs "25.1"))
  :commit "ca2cffb0b174e9d814ad95178af84b525dd2b64d" :authors
  '(("Gaby Launay" . "gaby.launay@protonmail.com"))
  :maintainers
  '(("Gaby Launay" . "gaby.launay@protonmail.com"))
  :maintainer
  '("Gaby Launay" . "gaby.launay@protonmail.com")
  :keywords
  '("python" "tools")
  :url "https://github.com/galaunay/poetry.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
