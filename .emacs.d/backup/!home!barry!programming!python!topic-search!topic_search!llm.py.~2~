from pathlib import Path
import wget
import torch
from langchain.llms import LlamaCpp
from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler


class FrozenLlama:
    def __init__(self, url: str, temperature=0.1, max_tokens=1024, n_batch=512, n_gpu_layers=40, seed=69, top_p=1, root_dir="./", verbose=False):
        self.root_dir = Path(root_dir)
        self.weights: Path = self.download_llama_model_if_missing(url)
        self.temperature = temperature
        self.max_tokens = max_tokens
        self.n_batch = n_batch
        self.n_gpu_layers = n_gpu_layers
        self.seed = seed
        self.top_p = top_p
        self.verbose = verbose

        self.llama = None
        self.callback = None

    def __call__(self, query: str) -> str:
        if self.llama is None: # Lazy load model check
            self._load_model()
        return self.llama(query)

    def _load_model(self):
        logger.debug("loading LLaMa2 model...")
        self.callback = CallbackManager([StreamingStdOutCallbackHandler()])
        self.llama = LlamaCpp(
            model_path=str(self.weights),
            temperature=self.temperature,
            n_gpu_layers=self.n_gpu_layers,
            n_batch=self.n_batch,
            max_tokens=self.max_tokens,
            top_p=self.top_p,
            callback=self.callback,
            seed=self.seed,
            verbose=self.verbose)

    def download_llama_model_if_missing(self, url: str) -> Path:
        path = self.root_dir / Path(url.split("/")[-1])
        if path.exists():
            logger.debug(f"found llm weights at: {str(path)}")
            return path
        logger.debug("cached llm weights not found, downloading")
        path.parent.mkdir(parents=True, exist_ok=True)
        try:
            wget.download(url, str(path))
        except Exception as e:
            print(f"An error occurred while downloading: {e}")
            return None
        logger.debug(f"Weights cached at: {str(path)}")
        return path
