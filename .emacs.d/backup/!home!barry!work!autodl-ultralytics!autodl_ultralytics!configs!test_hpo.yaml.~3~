# variables with underscores "_var_" are used internally and should not be modified directly
# as they can be overwritten
# _job_uid_: "" # (str) <- set by scheduler
# _search_variant_: ""  # (str) <- set by scheduler
# _search_uid_: ""  # (str) <- set by scheduler

context: 'vdl/smart-trim' # (REQUIRED|str) context (refers to the owner of the uploaded artifacts')
job_type: 'TRAIN' # (REQUIRED|str) type of job to run.
experiment_name: 'hpo-experiment-1'  # (Optional[str]) experiment to use. Required if search is used.

search: # only used when submitted as search
  enabled: true
  engine: 'ultralytics'
  metric: 'metrics/mAP50-95(B)'
  direction: 'minimize'
  algorithm: 'grid_search'
  grace_period: 50
  spaces:
    - "lr0" : {"loguniform" : [1e-5, 1e-1] }              # Initial learning rate
    - "lrf" : { "uniform" : [.01, 1.] }                   # Final learning rate factor
    - "momentum" : { "uniform" : [.6, .98]}              # Momentum
    - "weight_decay" : { "uniform" : [.0, .001]}         # Weight Decay
    #- "warmup_epochs" : { "uniform" : [.0, 5.0]}         # Warmup Epochs
    #- "warmup_momentum" : { "uniform" : [.0, .95]}       # Warmup Momentum
    #- "box" : { "uniform" : [.02, .2]}                   # Box loss weight
    #- "cls" : { "uniform" : [.2, 4.0]}                   # Class loss weight
    #- "hsv_h" : { "uniform" : [.0, .1]}                  # Hue augmentation range
    #- "hsv_s" : { "uniform" : [.0, .1]}                  # Saturation augmentation range
    #- "hsv_v" : { "uniform" : [.0, .2]}                  # Value (Brightness) augmentation range
    - "degrees" : { "uniform" : [.0, 45.0]}              # Rotation augmentation range (degrees)
    #- "translate" : { "uniform" : [.0, .9]}              # Translation augmentation range
    #- "scale" : { "uniform" : [.0, .9]}                  # Scaling augmentation range
    #- "shear" : { "uniform" : [.0, 10.]}                 # Shear augmentation range (degrees)
    #- "perspective" : { "uniform" : [.0, .001]}          # Perspective augmentation range
    #- "flipud" : { "uniform" : [.0, 1.]}                 # Vertical flip augmentation probability
    #- "fliplr" : { "uniform" : [.0, 1.]}                 # Horizontal flip augmentation probability
    - "mosaic" : { "uniform" : [.0, 1.]}                 # Mosaic augmentation probability
    - "mixup" : { "uniform" : [.0, 1.]}                  # Mix-Up augmentation probability
    - "copy_paste" : { "uniform" : [.0, 1.]}             # Copy-paste augmentation probability


algorithm: 'grid_search' # (REQUIRED|str) algorithm to use for search

docker:
  image: 'docker.prod.vbti.nl/test/autodl-ultralytics:1.53'  # (REQUIRED|str) docker image to use, e.g. 'docker.prod.vbti.nl/cloud/mmdet-train:v0.5.0'
  env: {'WANDB_API_KEY' : 'acf2d87d26168d9016a518427c249098c1c569d1'} # (Dict[str, str]) environment variables to set, e.g. {'PYTHONPATH': '/autodl-mono-repo'}
  ports: [] # (List[str]) ports to expose, e.g. ['8080:8080']

tracking:
  enabled: true # (bool) whether to enable tracking


model_name_suffix: 'yolo_test'
root_dir: '/tmp/' # only for testing, autodl-trainer-common will overwrite

storage:
  # set the environment variables correctly, see https://gitlab.com/vbti-products/autodl/autodl-storage-client/-/blob/main/autodl_storage_client/config/settings.yaml
  download_datasets: true  # (bool) enable storage integration
  upload_model: true # (bool) upload the model after training, the name of the model will be set to _run_dir_name_
  model_tags: [] # (List[str]) tags to use for the uploading of the
  model_notes: {} # (Dict[str, str]) notes to use for the uploading of the model

datasets: # datasets to use
  train: # training dataset
    name: instseg_train_v28
  val:
    name: instseg_val_v28
  test:
    name: instseg_val_v28


force_data_export: False # force overwrite of data export
framework: 'ultralytics' # (REQUIRED|str) framework to use, e.g. mmdetection
framework_config: # (Optional[dict]) framework specific config
  task: segment # YOLO task, i.e. detect, segment, classify, pose
  model: 'yolov8m-seg.yaml' # default: yolov8n.yaml, for segmentation: yolov8n-seg.yaml
  patience: 50 # epochs to wait for no observable improvement for early stopping of training
  batch: -1 # number of images per batch (-1 for AutoBatch)
  imgsz: 768
  workers: 8 # number of worler threads for data loading (per RANK if DDP)
  optimizer: SGD # optimizer to use, choices=['SGD', 'Adam', 'AdamW', 'RMSProp']
  verbose: True # whether to print verbose output
  seed: 69 # random seed for reproducibility
  deterministic: True
  single_cls: False # train multi-class data as a single-class
  rect: False  # rectangular training if mode='train' or rectangular validation if mode='val'
  cos_lr: False  # use cosine learning rate scheduler
  close_mosaic: 10  # (int) disable mosaic augmentation for final epochs
  resume: False
  amp: True  # Automatic Mixed Precision (AMP) training, choices=[True, False], True runs AMP check
  # Segmentation
  overlap_mask: True  # masks should overlap during training (segment train only)
  mask_ratio: 4  # mask downsample ratio (segment train only)
  scale: 0.4
  # Classification
  # dropout: 0.0 # use dropout regularization (classify train only)
  epochs: 250
