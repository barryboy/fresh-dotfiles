from typing import List
from copy import deepcopy
from pathlib import Path
from loguru import logger
from tqdm import tqdm

from ultralytics.data.annotator import auto_annotate

from autodl_datasets import Dataset, InstanceSegmentationDataset, ObjectDetectionDataset
from autodl_storage_client import MultiClient


def inst_seg_to_obj_det(dataset: InstanceSegmentationDataset)-> ObjectDetectionDataset:
    for i, (x, y) in enumerate(dataset.records):
        print(i, x,y)
        dataset.targets[i] = from_mask_to_bbox()
    pass


def handle_bbox(bbox, img_h, img_w, classes_mapping) -> List[float]:
    """
    input: bounding box object, image height, width in pixels
    output: Tuple(class, cx, cy, w, h) where class is the class mapped to int,
      and cx,cy,w,h are normalized
    """
    assert bbox["box"]["theta_le90"] == 0.0, f"Rotated bboxes not supported, found bbox with {bbox['box']['theta_le90']} degrees."
    b = [
        bbox["box"]["cx"],
        bbox["box"]["cy"],
        bbox["box"]["w"],
        bbox["box"]["h"],
    ]  # extract cx, cy, w, h
    normalized_bbox = (
        classes_mapping[bbox["label"]],
        b[0] / img_w,
        b[1] / img_h,
        b[2] / img_w,
        b[3] / img_h,
    )  #  add class & normalize
    return normalized_bbox


def main(dataset: ObjectDetectionDataset, det_model=None, force=False) -> InstanceSegmentationDataset:
    converted_data = get_converted_path(dataset)
    detection_model = det_model if det_model is not None else "yolov8x.pt"
    output_dir = converted_data.parent / "auto-annotated"
    auto_annotate(data=converted_data, det_model=detection_model, output_dir=output_dir)
    return from_yolo_to_adl(output_dir)


def from_yolo_to_adl(path: Path)-> InstanceSegmentationDataset:
    pass




def from_adl_inst_seg_to_yolo_obj_det_path(dataset: InstanceSegmentationDataset, root_dir: Path) -> None:
    (root_dir / "images").mkdir(parents=True, exist_ok=True)
    (root_dir / "labels").mkdir(parents=True, exist_ok=True)

    label_map = ref.label_map.as_dict()["class_id_to_label_map"]
    label_map = {v: k for k, v in label_map.items()}

    for i, (x, y) in enumerate(tqdm(ref)):
        output_file = root_dir / Path(f"labels/im{i}.txt")
        targets_content = [handle_bbox(bbox.as_dict(), x.height, x.width, label_map) for bbox in y.get_all_bboxes()]
        target_str = [(" ".join(map(str, target)) + "\n") for target in targets_content]
        with open(output_file, "w+") as f:
            f.write("\n".join(target_str))

        output_file = root_dir / Path(f"images/im{i}.png")
        x.write(output_file)

if __name__ == "__main__":
    root_dir = Path("/home/barry/autodl/tunz")

    dataset_conf = {"train": {"name": "instseg_train_v32"}, "test": {"name": "instseg_val_v32"}}
    storage = MultiClient("vdl/smart-trim")
    ref = storage.datasets.retrieve("instseg_val_v28", include_references=True)
    print(ref.label_map.as_dict()["class_id_to_label_map"])
    #from_adl_inst_seg_to_yolo_obj_det_path(ref, root_dir)

    print("autoannotating...")
    auto_annotate(data=root_dir / "images", det_model="wop.pt", sam_model="mobile_sam.pt", output_dir=(root_dir / "auto-annotated-good"))
