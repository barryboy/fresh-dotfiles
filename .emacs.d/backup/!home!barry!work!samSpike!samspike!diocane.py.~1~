from concurrent.futures import ProcessPoolExecutor, as_completed
import random
import os
from pathlib import Path

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import seaborn as sns
from tqdm import tqdm
import cv2


def parse_mask(mask_path: Path):
    with open(mask_path, "r") as f:
        mask_data = f.read()
    masks = []
    lines = mask_data.strip().split("\n")
    for line in lines:
        if not line.strip():
            parts = line.split()
            class_index = int(parts[0])
            coords = np.array(parts[1:], dtype=float).reshape(-1, 2)
            masks.append((class_index, coords))
    return masks


def create_binary_mask(height, width, polygon):
    mask = np.zeros((height, width), dtype=np.uint8)
    cv2.fillPoly(mask, [polygon], 255)
    return mask


def unnormalize(coords, img_width, img_height):
    xs, ys = coords[:, 0], coords[:, 1]
    xs = (xs * img_width).astype(int)
    ys = (ys * img_height).astype(int)
    return np.column_stack((xs, ys))


def match_masks(set1, set2, threshold=0.5):
    matched_pairs = []
    for i, mask1 in enumerate(set1):
        best_match = None
        best_iou = 0
        for j, mask2 in enumerate(set2):
            iou = calculate_iou(mask1, mask2)
            if iou > best_iou:
                best_iou = iou
                best_match = j
        if best_iou >= threshold:
            matched_pairs.append((i, best_match, best_iou))
    return matched_pairs


def calculate_iou(mask1, mask2):
    intersection = np.sum(np.logical_and(mask1, mask2))
    union = np.sum(np.logical_or(mask1, mask2))
    return np.nan if union == 0 else intersection / union


def calculate_dice_loss(mask1, mask2):
    return 1 - (2. * np.sum(np.logical_and(mask1, mask2)) + 1e-6) / (np.sum(mask1 + mask2) + 1e-6)


def run_eval_on_one_image(root_dir: Path, image: str):
    y_true = parse_mask(root_dir / "og_labels"/ (image+".txt"))
    y_pred = parse_mask(root_dir / "og_labels"/ (image+".txt"))
    img_h, img_w = plt.imread(str(root_dir / "images" / (image+".png"))).shape[:2]

    y_true_binary = [(class_idx, create_binary_mask(img_h, img_w, unnormalize(coords, img_w, img_h))) for class_idx, coords in y_true]
    y_pred_binary = [(class_idx, create_binary_mask(img_h, img_w, unnormalize(coords, img_w, img_h))) for class_idx, coords in y_pred]

    matched_pairs = match_masks([x[1] for x in y_true_binary], [x[1] for x in y_pred_binary])
    res = []
    for pair in matched_pairs:
        res.append({
            "image": image.split("m")[1],
            "cls": y_true[pair[0]][0],
            "iou": pair[2],
            "dice": calculate_dice_loss(y_true_binary[pair[0]][1], y_pred_binary[pair[1]][1])
        })
    return res


def run_eval(root_dir: Path, n_workers=8):
    img_dir = root_dir / "images"
    image_files = os.listdir(str(img_dir))

    backgrounds = ["im10.txt", "im2.txt", "im6.txt"]
    for item in backgrounds:
        if item in image_files:
            image_files.remove(item)

    results = []

    with ProcessPoolExecutor(max_workers=n_workers) as executor:
        futures = [executor.submit(run_eval_on_one_image, root_dir, image.split(".")[0]) for image in image_files]

        with tqdm(total=len(image_files)) as progress:
            for future in as_completed(futures):
                result = future.result()
                results.extend(result)
                progress.update(1)

    df = pd.DataFrame.from_records(results)
    return df


if __name__ == "__main__":
    root_dir = Path("/home/barry/work/samSpike/instseg_val_v28")
    run_eval(root_dir, n_workers=9)
