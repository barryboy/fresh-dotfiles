from autodl_datasets import InstanceSegmentationDataset, ObjectDetectionDataset
from autodl_storage_client import MultiClient


def _load_data(self) -> ObjectDetectionDataset:
        """
        Method to load dataset locally given valid autodl context, valid dataset(name) present in context.
        Returns:
          ObjectDetectionDataset: loaded from storage
        """
        logger.info(f"getting {self.context}/{self.dataset_name}...")
        storage = MultiClient(self.context)
        return storage.datasets.retrieve(self.dataset_name, include_references=True)

    def _convert_data_if_needed(self, force: bool) -> None:
        """
        Method to convert data only if there exist no folder named dataset_name under root_dir
        WARNING: not a fool proof caching method, it assumes that if the folder exist, the contents are good.
        This does not check the content of the folder, so delete root_dir/dataset_name to rerun conversion.
        Args:
          force: force data downloading and conversion
        """
        if (self.subdir / "images").exists() and not force:
            logger.info(f"Local converted dataset found at {self.subdir}, skipping conversion...")
            return
        else:
            logger.info("Converted dataset not found locally, starting conversion...")
            self._convert_data()

    def _convert_data(self) -> None:
        """
        Method to convert a single autodl OD dataset to YOLO format
        """
        (self.subdir / "images").mkdir(parents=True, exist_ok=True)
        (self.subdir / "labels").mkdir(parents=True, exist_ok=True)

        label_map = {v: k for k, v in self.label_map.items()}

        for i, (x, y) in enumerate(tqdm(self.od_dataset, desc="Saving dataset to YOLO format")):
            output_file = self.subdir / Path(f"labels/im{i}.txt")
            targets_content = [
                self._handle_bbox(bbox.as_dict(), x.height, x.width, label_map) for bbox in y.get_all_bboxes()
            ]
            target_str = [(" ".join(map(str, target)) + "\n") for target in targets_content]
            with open(output_file, "w+") as f:
                f.write("\n".join(target_str))

            output_file = self.subdir / Path(
                f"images/im{i}.png"
            )  # TODO verify this works with non png images, prolly not.
            x.write(output_file)
        logger.info(f"Saved converted dataset to {self.subdir}")

    def _handle_bbox(self, bbox: Dict, img_h: int, img_w: int, classes_mapping) -> List[float]:
        """
        Method to convert autodl formatted bboxes to YOLO format.
        Args:
          bbox (dict): dictionary of autodl BBox obj.
          img_h (int): image height in pixels
          img_w (int): image width in pixels
          classes_mapping (dict): dictionary of class names to class idxs
        Returns:
          Tuple(class_idx, cx, cy, w, h) where cx,cy,w,h are normalized according to Ultralytics format.
        """
        assert (
            bbox["box"]["theta_le90"] == 0.0
        ), f"Rotated bboxes not supported, found bbox with {bbox['box']['theta_le90']} degrees."
        b = [
            bbox["box"]["cx"],
            bbox["box"]["cy"],
            bbox["box"]["w"],
            bbox["box"]["h"],
        ]  # extract cx, cy, w, h
        normalized_bbox = (
            classes_mapping[bbox["label"]],
            b[0] / img_w,
            b[1] / img_h,
            b[2] / img_w,
            b[3] / img_h,
        )  #  add class & normalize
        return normalized_bbox
