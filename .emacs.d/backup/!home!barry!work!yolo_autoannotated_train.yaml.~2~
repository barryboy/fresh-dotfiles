# variables with underscores "_var_" are used internally and should not be modified directly
# as they can be overwritten
# _job_uid_: "" # (str) <- set by scheduler
# _search_variant_: ""  # (str) <- set by scheduler
# _search_uid_: ""  # (str) <- set by scheduler

context: 'vdl/smart-trim' # (REQUIRED|str) context (refers to the owner of the uploaded artifacts')
job_type: 'TRAIN' # (REQUIRED|str) type of job to run.
experiment_name: 'yolo8 autoannotated vs'  # (Optional[str]) experiment to use. Required if search is used.

search: # only used when submitted as search
  algorithm: 'grid_search' # (REQUIRED|str) algorithm to use for search

docker:
  image: 'docker.prod.vbti.nl/cloud/autodl-ultralytics-x86:0.2.4'  # (REQUIRED|str) docker image to use, e.g. 'docker.prod.vbti.nl/cloud/mmdet-train:v0.5.0'

# resources constraints for the job
resource_constraints:
  cpu_count: 12 # (int) number of cpus to use
  cpu_architecture:    # (Optional[str]) cpu architecture one of [amd64, arm64, armv7, x86] | NOTIMPLEMENTED
  gpu_count: 1 # (int) number of gpus to use

tracking:
  enabled: true # (bool) whether to enable tracking

model_name_suffix: 'yolo_test'
root_dir: '/tmp/' # only for testing, autodl-trainer-common will overwrite

storage:
  # set the environment variables correctly, see https://gitlab.com/vbti-products/autodl/autodl-storage-client/-/blob/main/autodl_storage_client/config/settings.yaml
  download_datasets: true  # (bool) enable storage integration
  upload_model: true # (bool) upload the model after training, the name of the model will be set to _run_dir_name_
  model_tags: [] # (List[str]) tags to use for the uploading of the
  model_notes: {} # (Dict[str, str]) notes to use for the uploading of the model

datasets: # datasets to use
  train: # training dataset
    name: instseg_train_v32
  val:
    name: instseg_val_v32
  test:
    name: instseg_val_v32

#load_from: # (Optional[dict]) load from a model
#  name: test/project_a/models/20230417-124258 # (Optional[str]) name of the model (in storage) to load from, will overwrite framework_config.load_from
#  model_dir: ~ # (Optional[str]) path to the model directory to load from, will overwrite framework_config.load_from
#  checkpoint_name: ~ # (Optional[str]) name of the checkpoint to load from, will overwrite framework_config.load_from

force_data_export: False # force overwrite of data export
framework: 'ultralytics' # (REQUIRED|str) framework to use, e.g. mmdetection
framework_config: # (Optional[dict]) framework specific config
  task: segment # YOLO task, i.e. detect, segment, classify, pose
  model: 'yolov8m-seg.yaml' # default: yolov8n.yaml, for segmentation: yolov8n-seg.yaml
  patience: 40 # epochs to wait for no observable improvement for early stopping of training
  batch: 16 # number of images per batch (-1 for AutoBatch)
  imgsz: 640
  workers: 8 # number of worler threads for data loading (per RANK if DDP)
  optimizer: AdamW # optimizer to use, choices=['SGD', 'Adam', 'AdamW', 'RMSProp']
  verbose: True # whether to print verbose output
  seed: 69 # random seed for reproducibility
  deterministic: True
  single_cls: False # train multi-class data as a single-class
  rect: False  # rectangular training if mode='train' or rectangular validation if mode='val'
  cos_lr: False  # use cosine learning rate scheduler
  close_mosaic: 20  # (int) disable mosaic augmentation for final epochs
  resume: False
  amp: True  # Automatic Mixed Precision (AMP) training, choices=[True, False], True runs AMP check
  # Segmentation
  overlap_mask: True  # masks should overlap during training (segment train only)
  mask_ratio: 4  # mask downsample ratio (segment train only)
  scale: 0.4
  # Classification
  # dropout: 0.0 # use dropout regularization (classify train only)
  epochs: 400
