from typing import Optional, Union

from autodl_model_zoo._ultralytics._base import UltralyticsTask, YOLOV8ModelSize, _BaseYOLOV8Config


class YOLOV8Config(_BaseYOLOV8Config):
    def __init__(
        self,
        model_name: str,
        train_dataset_name: str,
        val_dataset_name: Optional[str] = None,
        test_dataset_name: Optional[str] = None,
        model_size: Union[str, YOLOV8ModelSize] = YOLOV8ModelSize.YOLOv8N,
        num_epochs: int = 12,
        batch_size: int = 6,
    ):
        super().__init__(
            model_name=model_name,
            model_size=model_size,
            train_dataset_name=train_dataset_name,
            val_dataset_name=val_dataset_name,
            test_dataset_name=test_dataset_name,
            num_epochs=num_epochs,
            batch_size=batch_size,
        )
        self.framework_config.task = UltralyticsTask.segment

    def _model_size_to_model_str(self, model_size: YOLOV8ModelSize, pretrained: bool = True) -> str:
        return f"{model_size.lower()}-seg" + ".pt" if pretrained else ".yaml"
