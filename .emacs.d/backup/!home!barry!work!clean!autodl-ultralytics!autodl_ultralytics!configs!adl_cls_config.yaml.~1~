# variables with underscores "_var_" are used internally and should not be modified directly
# as they can be overwritten
# _job_uid_: "" # (str) <- set by scheduler
# _search_variant_: ""  # (str) <- set by scheduler
# _search_uid_: ""  # (str) <- set by scheduler

context: 'vdl/smart-trim' # (REQUIRED|str) context (refers to the owner of the uploaded artifacts')
job_type: 'TRAIN' # (REQUIRED|str) type of job to run.
experiment_name: 'yolo8 test'  # (Optional[str]) experiment to use. Required if search is used.

search: # only used when submitted as search
  algorithm: 'grid_search' # (REQUIRED|str) algorithm to use for search

docker:
  image: 'docker.prod.vbti.nl/cloud/autodl-ultralytics-x86:0.1.38'  # (REQUIRED|str) docker image to use, e.g. 'docker.prod.vbti.nl/cloud/mmdet-train:v0.5.0'
  env: {} # (Dict[str, str]) environment variables to set, e.g. {'PYTHONPATH': '/autodl-mono-repo'}
  ports: [] # (List[str]) ports to expose, e.g. ['8080:8080']

# resources constraints for the job
resource_constraints:
  node_name:   # (Optional[str]) name of the node to use | NOTIMPLEMENTED
  cpu_count: 1 # (int) number of cpus to use
  cpu_architecture:    # (Optional[str]) cpu architecture one of [amd64, arm64, armv7, x86] | NOTIMPLEMENTED
  gpu_count: 1 # (int) number of gpus to use
  gpu_model:   # Optional[str]) gpu model to use
  gpu_memory:   # Optional[str]) gpu memory to use
  gpu_architecture:   # (Optional[str]) gpu architecture | NOTIMPLEMENTED

tracking:
  enabled: true  # (bool) whether to enable tracking
  # wandb: ~
  # when using wandb, set the api key in the environment variable WANDB_API_KEY
  # wandb: # check check https://github.com/open-mmlab/mmcv/blob/master/mmcv/runner/hooks/logger/wandb.py
  #   init_kwargs:  # check https://docs.wandb.ai/ref/python/init for more info
  #     project: 'project_name' # (str) <- overwritten by "project", and ignored
  #     entity:   # (str) who is logging the run, should exist in the project
  #     config:    # important values to save with the run, _search_variant_ will be appended here
  #     save_code: false # (bool) save the commit hash of the code
  #     group:   # (str) <- overwritten if _experiment_name_ is set
  #     job_type: train # (str) type of job to track
  #     tags:   # (str) tags to use
  #     name:   # (str) <- overwritten if _job_uid is set
  #     notes:   # (str) notes to add to the run
  #     magic: false # (bool) enable wandb magic
  #     anonymous: never # <- hardcoded and ignored, set to never
  #     mode: online # <- hardcoded and ignored, set to online
  #     force: true # <- hardcoded and ignored, set to True
  #     sync_tensorboard: false # (bool) sync tensorboard logs
  #     resume:   # (bool) resume a run
  #     id:   # (str) id of the run to resume
  #   interval: 10  # (int) interval to log metrics
  #   by_epoch: true  # (bool) Whether EpochBasedRunner is used. Default: True.
  #   log_artifact: false # (bool) <- hardcoded and ignored, set to false


model_name_suffix: 'yolo_test'
root_dir: '/tmp/' # only for testing, autodl-trainer-common will overwrite

storage:
  # set the environment variables correctly, see https://gitlab.com/vbti-products/autodl/autodl-storage-client/-/blob/main/autodl_storage_client/config/settings.yaml
  download_datasets: true  # (bool) enable storage integration
  upload_model: true # (bool) upload the model after training, the name of the model will be set to _run_dir_name_
  model_tags: [] # (List[str]) tags to use for the uploading of the
  model_notes: {} # (Dict[str, str]) notes to use for the uploading of the model

datasets: # datasets to use
  train: # training dataset
    name: instseg_val_v18
  val:
    name: instseg_val_v18
  test:
    name: instseg_val_v18

#load_from: # (Optional[dict]) load from a model
#  name: test/project_a/models/20230417-124258 # (Optional[str]) name of the model (in storage) to load from, will overwrite framework_config.load_from
#  model_dir: ~ # (Optional[str]) path to the model directory to load from, will overwrite framework_config.load_from
#  checkpoint_name: ~ # (Optional[str]) name of the checkpoint to load from, will overwrite framework_config.load_from

force_data_export: False # force overwrite of data export
framework: 'ultralytics' # (REQUIRED|str) framework to use, e.g. mmdetection
framework_config: # (Optional[dict]) framework specific config
  task: segment # YOLO task, i.e. detect, segment, classify, pose
  model: 'yolov8n-seg.yaml' # default: yolov8n.yaml, for segmentation: yolov8n-seg.yaml
  patience: 50 # epochs to wait for no observable improvement for early stopping of training
  batch: 1 # number of images per batch (-1 for AutoBatch)
  imgsz: 320
  workers: 8 # number of worler threads for data loading (per RANK if DDP)
  optimizer: SGD # optimizer to use, choices=['SGD', 'Adam', 'AdamW', 'RMSProp']
  verbose: True # whether to print verbose output
  seed: 69 # random seed for reproducibility
  deterministic: True
  single_cls: False # train multi-class data as a single-class
  rect: False  # rectangular training if mode='train' or rectangular validation if mode='val'
  cos_lr: False  # use cosine learning rate scheduler
  close_mosaic: 0  # (int) disable mosaic augmentation for final epochs
  resume: False
  amp: True  # Automatic Mixed Precision (AMP) training, choices=[True, False], True runs AMP check
  # Segmentation
  overlap_mask: True  # masks should overlap during training (segment train only)
  mask_ratio: 4  # mask downsample ratio (segment train only)
  scale: 0.4
  # Classification
  # dropout: 0.0 # use dropout regularization (classify train only)
  epochs: 2
