from pprint import pprint
from typing import Optional, Union

from autodl.zoo._ultralytics._base import YOLOV8ModelSize, _BaseYOLOV8Config

class YOLOV8Config(_BaseYOLOV8Config):
    def __init__(
            self,
            train_dataset_name: str,
            val_dataset_name: Optional[str] = None,
            test_dataset_name: Optional[str] = None,
            model_size: Union[str, YOLOV8ModelSize] = YOLOV8ModelSize.YOLOv8N,
            num_epochs: int = 12,
            batch_size: int = 6,
    ):
        super().__init__(
            model_size=model_size,
            train_dataset_name=train_dataset_name,
            val_dataset_name=val_dataset_name,
            test_dataset_name=test_dataset_name,
            num_epochs=num_epochs,
            batch_size=batch_size,
        )
        self.framework_config.task = UltralyticsTask.classify


    def _model_size_to_model_str(self, model_size: YOLOV8ModelSize, pretrained: bool = True) -> str:
        return f"{model_size.lower()}-cls" + ".pt" if pretrained else ".yaml"


if __name__ == "__main__":
    yolov8_config = YOLOV8Config(train_dataset_name="testing_dataset_classification"
    )
    pprint(yolov8_config.model_json_schema())
    yolov8_config.framework_config.cache=False
    yolov8_config.framework_config.epochs=1
