"""
File: eval.py
Description: Provides tools for evaluating 2 sets of YOLO formatted segmentation masks, returns IoU, Dice Loss per mask, class.
             It converts to binary masks, matches the 2 sets to find the correct pairs to evaluate, runs metrics on all masks.

Functions:
    parse_mask - Parses mask data from a file.
    create_binary_mask - Creates a binary mask from polygon coordinates.
    unnormalize - Converts normalized coordinates to actual image coordinates.
    match_masks - Matches masks from two sets based on IoU.
    calculate_iou - Calculates the IoU between two masks.
    calculate_dice_loss - Calculates the Dice loss between two masks.
    run_eval_on_one_image - Runs evaluation on a single image.
    run_eval - Runs evaluation on a set of images in a directory, multithreaded.

"""

from typing import List, Tuple, Dict
import os
from pathlib import Path
from concurrent.futures import ProcessPoolExecutor, as_completed
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm
import cv2


def parse_mask(mask_path: Path) -> List[Tuple[int, np.ndarray]]:
    """
    Parses mask data from a file.

    Args:
        mask_path (Path): The path to the mask file.

    Returns:
        List[Tuple[int, np.ndarray]]: A list of tuples containing class index and coordinates.
    """
    with open(mask_path, "r") as f:
        mask_data = f.read()
    masks = []
    lines = mask_data.strip().split("\n")
    for line in lines:
        parts = line.split()
        class_index = int(parts[0])
        coords = np.array(parts[1:], dtype=float).reshape(-1, 2)
        masks.append((class_index, coords))
    return masks


def create_binary_mask(height: int, width: int, polygon: np.ndarray) -> np.ndarray:
    """
    Creates a binary mask for a given polygon.

    Args:
        height (int): The height of the image.
        width (int): The width of the image.
        polygon (np.ndarray): The polygon coordinates.

    Returns:
        np.ndarray: The binary mask.
    """
    mask = np.zeros((height, width), dtype=np.uint8)
    cv2.fillPoly(mask, [polygon], 255)
    return mask


def unnormalize(coords: np.ndarray, img_width: int, img_height: int) -> np.ndarray:
    """
    Converts normalized coordinates to actual image coordinates.

    Args:
        coords (np.ndarray): Normalized coordinates.
        img_width (int): Width of the image.
        img_height (int): Height of the image.

    Returns:
        np.ndarray: Unnormalized coordinates.
    """
    xs, ys = coords[:, 0], coords[:, 1]
    xs = (xs * img_width).astype(int)
    ys = (ys * img_height).astype(int)
    return np.column_stack((xs, ys))


def match_masks(
    set1: List[np.ndarray], set2: List[np.ndarray], threshold: float = 0.5
) -> List[Tuple[int, int, float]]:
    """
    Matches masks from two sets based on IoU.

    Args:
        set1 (List[np.ndarray]): First set of masks.
        set2 (List[np.ndarray]): Second set of masks.
        threshold (float): IoU threshold for matching.

    Returns:
        List[Tuple[int, int, float]]: Matched pairs of indexes and their IoU.
    """
    matched_pairs = []
    for i, mask1 in enumerate(set1):
        best_match = None
        best_iou = 0
        for j, mask2 in enumerate(set2):
            iou = calculate_iou(mask1, mask2)
            if iou > best_iou:
                best_iou = iou
                best_match = j
        if best_iou >= threshold:
            matched_pairs.append((i, best_match, best_iou))
    return matched_pairs


def calculate_iou(mask1: np.ndarray, mask2: np.ndarray) -> float:
    """
    Calculates the Intersection over Union (IoU) of two masks.

    Args:
        mask1 (np.ndarray): First mask.
        mask2 (np.ndarray): Second mask.

    Returns:
        float: The IoU value.
    """
    intersection = np.sum(np.logical_and(mask1, mask2))
    union = np.sum(np.logical_or(mask1, mask2))
    return np.nan if union == 0 else intersection / union


def calculate_dice_loss(mask1: np.ndarray, mask2: np.ndarray) -> float:
    """
    Calculates the Dice loss between two masks.

    Args:
        mask1 (np.ndarray): First mask.
        mask2 (np.ndarray): Second mask.

    Returns:
        float: The Dice loss value.
    """
    return 1 - (2.0 * np.sum(np.logical_and(mask1, mask2)) + 1e-6) / (
        np.sum(mask1 + mask2) + 1e-6
    )


def run_eval_on_one_image(root_dir: Path, image: str) -> List[Dict[str, float]]:
    """
    Runs evaluation on a single image.

    Args:
        root_dir (Path): Root directory containing images and masks.
        image (str): Image file name.

    Returns:
        List[Dict[str, float]]: Evaluation results for the image.
    """
    y_true = parse_mask(root_dir / "og_labels" / (image + ".txt"))
    y_pred = parse_mask(root_dir / "og_labels" / (image + ".txt"))
    img_h, img_w = plt.imread(str(root_dir / "images" / (image + ".png"))).shape[:2]

    y_true_binary = [
        (class_idx, create_binary_mask(img_h, img_w, unnormalize(coords, img_w, img_h)))
        for class_idx, coords in y_true
    ]
    y_pred_binary = [
        (class_idx, create_binary_mask(img_h, img_w, unnormalize(coords, img_w, img_h)))
        for class_idx, coords in y_pred
    ]

    matched_pairs = match_masks(
        [x[1] for x in y_true_binary], [x[1] for x in y_pred_binary]
    )
    res = []
    for pair in matched_pairs:
        res.append(
            {
                "image": image.split("m")[1],
                "cls": y_true[pair[0]][0],
                "iou": pair[2],
                "dice": calculate_dice_loss(
                    y_true_binary[pair[0]][1], y_pred_binary[pair[1]][1]
                ),
            }
        )
    return res


def run_eval(root_dir: Path, n_workers: int = 8) -> pd.DataFrame:
    """
    Runs evaluation on all images in the specified directory, multithreaded.

    Args:
        root_dir (Path): The root directory containing images and labels.
        n_workers (int): Number of worker processes to use.

    Returns:
        pd.DataFrame: A DataFrame containing the evaluation results.
    """
    img_dir = root_dir / "images"
    image_files = os.listdir(str(img_dir))
    results = []

    with ProcessPoolExecutor(max_workers=n_workers) as executor:
        futures = [
            executor.submit(run_eval_on_one_image, root_dir, image.split(".")[0])
            for image in image_files
        ]

        with tqdm(total=len(image_files)) as progress:
            for future in as_completed(futures):
                result = future.result()
                results.extend(result)
                progress.update(1)

    df = pd.DataFrame.from_records(results)
    return df


if __name__ == "__main__":
    root_dir = Path("/home/barry/work/samSpike/instseg_val_v28")
    run_eval(root_dir, n_workers=9)
