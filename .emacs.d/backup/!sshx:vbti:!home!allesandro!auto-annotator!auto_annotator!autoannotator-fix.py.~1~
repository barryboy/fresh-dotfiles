import os
from pathlib import Path
from typing import Dict, List, Optional, Union

from aenum import StrEnum
from autodl_core import BBox, InstanceSegmentationInstances, PolygonMask
from autodl_datasets import (
    ImageDataset,
    InstanceSegmentationDataset,
    ObjectDetectionDataset,
)
from autodl_storage_client import MultiClient
from loguru import logger
from tqdm import tqdm
from ultralytics.data.annotator import auto_annotate


class SAM(StrEnum):
    """
    Enum class containing mapping to all supported sam model sizes.
    In ascending size order: MOBILE < B < L < H
    """

    MOBILE = "mobile_sam.pt"
    B = "sam_b.pt"
    L = "sam_l.pt"
    H = "sam_h.pt"


class AutoAnnotator:
    """
    Utility class to automatically generate masks given bounding boxes with Segment Anything Model(s) (SAM).
    This is used to make instance segmentation datasets out of object detection datasets without human labeling.
    """
    def __init__(
        self,
        context: str,
        od_dataset: str,
        od_model: Optional[Union[str, Path]] = None,
        sam_model: Optional[Union[str, SAM]] = "sam_b.pt",
        root_dir: Union[str, Path] = "~/autodl/",
        device: Union[str, int] = 1
    ):
        """
        Loads object detection dataset as attribute from given context / dataset_name and inits auto-annotator obj.
        Args:
          context (str): autodl context/project string, must be valid.
          od_dataset (str): name of object detection dataset present in given context. must be valid.
          model (Optional[str,Path]): specify path to .pt weights of YOLO model to use as backend to generate bboxes.
          sam_model (Optional[str, SAM]): set to specify which pretrained SAM model to use.
          root_dir (str, Path): directory where to download dataset and save annotations.
          device (Union[str, int]): either "cpu" or int corresponding to gpu card number.
        """
        self.device = device
        self.context = context
        self.dataset_name = od_dataset

        self.od_model =(
            od_model if od_model is not None else "yolov8m.pt"
        )  # if trained backend not provided, default to pretrained
        if type(sam_model) is SAM:
            self.sam_model = sam_model
        elif type(sam_model) is str:
            try:
                self.model = SAM(sam_model)
            except ValueError:
                raise ValueError(f"model {sam_model} not in supported models: {', '.join(option.value for option in SAM)}")
        else:
            raise TypeError(
                f"sam_model {sam_model} must be an instance of SAM Enum or a model name in allowed names: {', '.join(option.value for option in SAM)}"
            )
        self.od_dataset: ObjectDetectionDataset = self._load_data()
        self.label_map = self.od_dataset.label_map.as_dict()["class_id_to_label_map"]

        self.root_dir = Path(root_dir).expanduser()
        self.root_dir.mkdir(parents=True, exist_ok=True)
        self.subdir = self.root_dir / self.dataset_name
        self.subdir.mkdir(parents=True, exist_ok=True)
        self.labels_dir = self.subdir / "auto-annotated"
        self.labels_dir.mkdir(parents=True, exist_ok=True)

    def run(self, force=False) -> InstanceSegmentationDataset:
        """
        Main method to transform OD dataset to IS
        Args:
          force: force data downloading and conversion
        Returns:
          InstanceSegmentationDatset: auto-annotated IS dataset from OD dataset
        """
        self._convert_data_if_needed(force)
        self._to_instance_segmentation()
        return self._from_yolo_to_adl_inst_seg()

    def _load_data(self) -> ObjectDetectionDataset:
        """
        Method to load dataset locally given valid autodl context, valid autodl dataset(name) present in context.
        Returns:
          ObjectDetectionDataset: loaded from storage
        """
        logger.info(f"getting {self.context}/{self.dataset_name}...")
        storage = MultiClient(self.context)
        return storage.datasets.retrieve(self.dataset_name, include_references=True)

    def _convert_data_if_needed(self, force: bool) -> None:
        """
        Method to convert data only if there exist no folder named dataset_name under root_dir
        WARNING: this is not a fool proof caching method, but a util that assumes that if the folder exist, the contents are good.
        This does not check the content of the folder, so delete root_dir/dataset_name to rerun conversion.
        Args:
          force: force data downloading and conversion
        """
        if (self.subdir / "images").exists() and not force:
            logger.info(f"Local converted dataset found at {self.subdir}, skipping conversion...")
            return
        else:
            logger.info("Converted dataset not found locally, starting conversion...")
            self._convert_data()

    def _convert_data(self) -> None:
        """
        Method to convert a single autodl OD dataset to YOLO format
        """
        (self.subdir / "images").mkdir(parents=True, exist_ok=True)
        (self.subdir / "labels").mkdir(parents=True, exist_ok=True)

        label_map = {v: k for k, v in self.label_map.items()}

        for i, (x, y) in enumerate(
            tqdm(self.od_dataset, desc="Saving dataset to YOLO format")
        ):
            output_file = self.subdir / Path(f"labels/im{i}.txt")
            targets_content = [
                self._handle_bbox(bbox.as_dict(), x.height, x.width, label_map)
                for bbox in y.get_all_bboxes()
            ]
            target_str = [
                (" ".join(map(str, target)) + "\n") for target in targets_content
            ]
            with open(output_file, "w+") as f:
                f.write("\n".join(target_str))
            output_file = self.subdir / Path(
                f"images/im{i}.png"
            )  # TODO verify this works with non png images, prolly not.
            x.write(output_file)
        logger.info(f"saved converted dataset to {self.subdir}")

    def _handle_bbox(
        self, bbox: Dict, img_h: int, img_w: int, classes_mapping
    ) -> List[float]:
        """
        Method to convert autodl formatted bboxes to YOLO format.
        Args:
          bbox (dict): dictionary of autodl BBox obj.
          img_h (int): image height in pixels
          img_w (int): image width in pixels
          classes_mapping (dict): dictionary of class names to class idxs
        Returns:
          Tuple(class_idx, cx, cy, w, h) where cx,cy,w,h are normalized according to Ultralytics format.
        """
        assert (
            bbox["box"]["theta_le90"] == 0.0
        ), f"Rotated bboxes not supported, found bbox with {bbox['box']['theta_le90']} degrees."
        b = [
            bbox["box"]["cx"],
            bbox["box"]["cy"],
            bbox["box"]["w"],
            bbox["box"]["h"],
        ]  # extract cx, cy, w, h
        normalized_bbox = (
            classes_mapping[bbox["label"]],
            b[0] / img_w,
            b[1] / img_h,
            b[2] / img_w,
            b[3] / img_h,
        )  #  add class & normalize
        return normalized_bbox

    def _to_instance_segmentation(self) -> None:
        """
        Method that calls ultralytics auto_annotate obj to infer det model, pipe the output to sam and store sam output as labels file.
        """
        logger.info("starting auto-annotation...")
        auto_annotate(
            data=self.subdir / "images",
            det_model=self.od_model,
            sam_model=self.sam_model,
            output_dir=self.labels_dir,
            device=self.device,
        )

    def _from_yolo_to_adl_inst_seg(self) -> InstanceSegmentationDataset:
        """
        Method to convert results saved to folder in YOLO format to adl IS dataset.
        Returns:
          InstanceSegmentationDataset: IS dataset created from masks generated from bounding boxes with SAM
        """
        logger.info("converting results to adl InstanceSegmentationDataset obj...")
        image_dataset = ImageDataset.import_from_dir(self.subdir / "images")
        instseg_instances: List[InstanceSegmentationInstances] = []
        for i, record in enumerate(tqdm(image_dataset.records)):
            raw_instances: List[Dict[str, Union[BBox, PolygonMask]]] = []
            img_w, img_h = image_dataset[i][0].width, image_dataset[i][0].height
            with open(
                self.labels_dir / (record["image"].get_path().stem + ".txt")
            ) as f:
                for mask_i, line in enumerate(f):  # for every mask in file
                    if not line.strip():
                        continue  # skip empty lines
                    mask_data = line.strip().split()
                    class_idx = int(mask_data[0])  # get target
                    class_name = self.label_map.get(int(class_idx), "Unknown")
                    polygon = list(map(float, mask_data[1:]))
                    # denormalize
                    xs = [x * img_w for x in polygon[::2]]
                    ys = [y * img_h for y in polygon[1::2]]
                    # format poly in List[List[int]]
                    polys = [[item for sublist in zip(xs, ys) for item in sublist]]
                    polygon_mask = PolygonMask(polys, width=img_w, height=img_h)
                    bbox = BBox(polygon_mask.generate_minimal_box(), label=class_name)
                    raw_instances.append({"bbox": bbox, "mask": polygon_mask})
                logger.warning(raw_instances)
                inst_seg_instance = InstanceSegmentationInstances(raw_instances)
                logger.warning(inst_seg_instance)
                instseg_instances.append(inst_seg_instance)
        return image_dataset.to_instance_segmentation(instseg_instances)


if __name__ == "__main__":
    root_dir = Path("/home/allesandro/BagOData/")
    annotator = AutoAnnotator(
        context="vdl/smart-trim",
        od_dataset="instseg_val_v32",
        od_model="wop.pt",
        sam_model=SAM.B,
        root_dir=root_dir,
        device="cpu"
    )
    #annotator.run()
    df = annotator._from_yolo_to_adl_inst_seg()
