/* 
 my colorscheme generated from my wallpaper
 * */

static const char norm_fg[] = "#cfbdd7";
static const char norm_bg[] = "#171637";
static const char norm_border[] = "#cfbdd7"; // #908496 

static const char sel_fg[] = "#cfbdd7";
static const char sel_bg[] = "#9875C6";
static const char sel_border[] = "#00FFFF"; // #00ffff #00ffee #22e2ff

static const char urg_fg[] = "#cfbdd7";
static const char urg_bg[] = "#CF749E";
static const char urg_border[] = "#CF749E";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
};
